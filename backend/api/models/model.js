'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var EventsSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true
  },
  participants: {
    type: String,
    required: true
  },
  notes: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  created_date: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model('Events', EventsSchema);


var TemperaturesSchema = new Schema({
  temperature: {
    type: Number,
    required: true
  },
  created_date: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model('Temperature', TemperaturesSchema);