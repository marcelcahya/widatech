'use strict';
var mongoose = require('mongoose'), 
 Temperature = mongoose.model('Temperature'),
  Events = mongoose.model('Events');

exports.list_all_events = function(req, res) {
    Events.find({}, function(err, value) {
    if (err)
      res.send(err);
    res.json(value);
  });
};

exports.create_a_events = function(req, res) {
  var new_events = new Events(req.body);
  new_events.save(function(err, value) {
    if (err)
      res.send(err);
    res.json(value);
  });
};
 

exports.list_all_temperatures = function(req, res) {
    Temperature.find({}, function(err, value) {
    if (err)
      res.send(err);
    res.json(value);
  });
};

exports.create_a_temperatures = function(req, res) {
  var new_temperature = new Temperature(req.body);
  new_temperature.save(function(err, value) {
    if (err)
      res.send(err);
    res.json(value);
  });
};
