'use strict';
module.exports = function (app) {
  var controller = require('../controllers/controller');

  app.route('/events')
    .get(controller.list_all_events)
    .post(controller.create_a_events);

  app.route('/temperatures')
    .get(controller.list_all_temperatures)
    .post(controller.create_a_temperatures);

};
