# How to run backend

1.Get into the backend folder, run command "cd /backend"

2.Inside "backend" folder run "npm install" then "mongod"

3.Open/ start a new terminal, inside "backend" directory run "npm start"

4.Backend will running in localhost:3000


# How to run frontend:

1.Open/ start a new terminal

1.Get into the frontend folder, run command "cd /backend"

2.Inside "frontend" folder, run "npm install" then "npm start"

3.React.js will ask to run in a new terminal, press Y

4.Now React running in localhost:3001


# Want to erase database items?

1.Get into the backend folder, run command "mongo"

2.run command "use Eventsdb"

3.then run command "db.dropDatabase();"


# How to Run Automated Script for Live Chart:

1.Install Postman

2.Create POST request "http://localhost:3000/temperatures"

------------------------------------------------------------
3.Paste this script in the "Pre-request Script" config

var temperatures = pm.environment.get("temperatures");

if(!temperatures){
    temperatures = [31.81,30.65,34.53,37.66,24.22,17.74,33.37,21.09,33.38,17.56,42.75,42.89,23.55,22.37,26.66,29.74,25.23,33.17,28.17,42.13,26.34,26.01,19.82,24.22,40.43,30.99,27.72,28.29,18.82,33.14,29.74,17.05,19.22,16.58,39.65,37.72,30.77,26.44,21.6,31.07,34.64,31.8,23.36,20.07,31.06,42.88,30.27,31.72,38.78,43.23,39.11,28.9,21.41,40.28,15.93,20.06,39.2,19.47,15.07,40.49,21.47,42.84,27.77,21.29,24.77,26.03,33.82,39.04,40,36.73,16.83,30.27,26.21,32.28,15.96,32.38,39.03,22.73,38.25,16.34,38.7,28.34,23.41,41.78,34.45,44,33.03,15.99,40.81,17.72,42.73,41.55,44.07,23.09,29.83,25.75,33.08,44.57,39.84,33.32];

}
var currTemperature = temperatures.shift();
pm.environment.set("temperature", currTemperature);
pm.environment.set("temperatures", temperatures);


------------------------------------------------------------

4.Then paste this scripts in the "Tests" config

var temperatures = pm.environment.get("temperatures");

if(temperatures && temperatures.length > 0){
    postman.setNextRequest("http://localhost:3000/temperatures");
} else {
    postman.setNextRequest(null);
}

------------------------------------------------------------

5.Open "Runner" in the bottom right of Postman Application

6.Drag POST request to the "RUN ORDER"

7.Settings:

    - Set Iteration up to 100 times 
    
    - Set Delay to 10000 ms
    
    - uncheck "keep variable values"
    
    
8.Run New Collection.

9.New data will updated every 10 seconds to the live chart.