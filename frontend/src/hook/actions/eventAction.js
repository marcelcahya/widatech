import {
  EVENT_STARTED,
  EVENT_FAILURE,
  GET_EVENT_SUCCESS,
  ADD_EVENT_SUCCESS, 
} from "../constants/actionTypes";
import AxiosService from "../service/index"
 

export const addEvent = (payload) => async dispatch => {
  function onSuccess(events) {
    dispatch({ type: ADD_EVENT_SUCCESS, payload: { events } })
    return events
  }
  try {
    dispatch(onStart())
    const response = await AxiosService.create(payload)
    return onSuccess(response.data)
  }
  catch (e) {
    return dispatch(onError(e))
  }
}

export const getEvent = () => async dispatch => {
  function onSuccess(events) {
    dispatch({ type: GET_EVENT_SUCCESS, payload: { events } })
    return events
  }
  try {
    dispatch(onStart())
    const response = await AxiosService.getAll()
    return onSuccess(response.data)
  }
  catch (e) {
    return dispatch(onError(e))
  }
}

const onStart = () => {
  return {
    type: EVENT_STARTED
  };
};

const onError = error => {
  return {
    type: EVENT_FAILURE,
    payload: {
      error
    }
  };
};
