import {
    TEMP_STARTED,
    TEMP_FAILURE,
    GET_TEMP_SUCCESS
  } from "../constants/actionTypes";
  import AxiosService from "../service/index"
    
  
  export const getTemperature = () => async dispatch => {
    function onSuccess(temperatures) {
      dispatch({ type: GET_TEMP_SUCCESS, payload: { temperatures } })
      return temperatures
    }
    try {
      dispatch(onStart())
      const response = await AxiosService.getAllTemp()
      return onSuccess(response.data)
    }
    catch (e) {
      return dispatch(onError(e))
    }
  }
  
  const onStart = () => {
    return {
      type: TEMP_STARTED
    };
  };
  
  const onError = error => {
    return {
      type: TEMP_FAILURE,
      payload: {
        error
      }
    };
  };
  