import {
    TEMP_STARTED,
    TEMP_FAILURE,
    GET_TEMP_SUCCESS 
} from "../constants/actionTypes";

const initialState = {
    temperatures: [],
    loading: false,
    error: null
};

function tempReducer(state = initialState, action) {
    const { type, payload } = action

    switch (type) {
        case TEMP_STARTED:
            return {
                ...state,
                loading: true
            }
        case TEMP_FAILURE:
            return {
                ...state,
                loading: false,
                error: payload.error
            }
        case GET_TEMP_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                temperatures: payload.temperatures
            }  
        default:
            return state
    }
}

export default tempReducer;