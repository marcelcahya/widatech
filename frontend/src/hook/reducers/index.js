import { combineReducers } from 'redux';

import eventReducer  from './eventReducer'
import tempReducer from './temperatureReducer'

export default combineReducers({
    eventReducer,
    tempReducer
  });