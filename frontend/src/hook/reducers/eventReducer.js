import {
    EVENT_STARTED,
    EVENT_FAILURE,
    GET_EVENT_SUCCESS,
    ADD_EVENT_SUCCESS
} from "../constants/actionTypes";

const initialState = {
    events: [],
    loading: false,
    error: null
};

function eventReducer(state = initialState, action) {
    const { type, payload } = action

    switch (type) {
        case EVENT_STARTED:
            return {
                ...state,
                loading: true
            }
        case EVENT_FAILURE:
            return {
                ...state,
                loading: false,
                error: payload.error
            }
        case GET_EVENT_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                events: payload.events
            } 
        case ADD_EVENT_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                events: state.events.concat(payload.events)
            }
        default:
            return state
    }
}

export default eventReducer;