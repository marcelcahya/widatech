import http from "./http";

class AxiosService {
  getAll() {
    return http.get("/events");
  }
  get(id) {
    return http.get(`/events/${id}`);
  }
  create(payload) { 
    return http.post("/events", payload);
  }
  getAllTemp() {
    return http.get("/temperatures");
  }
}
export default new AxiosService();