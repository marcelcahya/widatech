import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux';
import store from './hook/stores/index';
import App from './components/App'; 

import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/index.css'

render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  );