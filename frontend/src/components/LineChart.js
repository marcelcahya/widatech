import React, { Component } from "react"
import { connect } from 'react-redux'
import { Line } from "react-chartjs-2"
import { getTemperature } from '../hook/actions/tempAction'
import Chart from 'chart.js/auto'
import zoom from "chartjs-plugin-zoom"
Chart.register(zoom);

class ConnectLineChart extends Component {
  componentDidMount() {
    this.props.getTemperature()
    this.timer = setInterval(() => this.props.getTemperature(), 10000)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
    this.timer = null;
  }

  render() {
    const { temperatures, loading, error } = this.props

    let labels = temperatures.map(el =>
      `${el.created_date.substr(0, 10)} ${el.created_date.substr(11, 8)}`)
    let data = temperatures.reverse().map(el => el.temperature)

    return (
      <div style={{ height: 700, overflowX: "scroll" }}>
        {loading && <div>LOADING ....</div>}
        {error && <div>{error}</div>}
        <Line
          redraw={true}
          ref={this.chartReference}
          data={{
            labels: labels,
            datasets: [
              {
                label: "Temperature",
                data: data, 
                backgroundColor: "rgba(121, 226, 255, 0.29)",
                borderColor: 'rgba(121, 226, 255, 0.8)', 
                fill: true,
              }
            ]
          }}
          options={{
            maintainAspectRatio: false,
            response: true,
            title: {
              display: true,
              text: "Temperature Live Chart"
            },
            elements: {
              line: {
                tension: 0.2 // disables bezier curves
              },
              point: {
                radius: 2
              }
            },
            scales: {
              x: {
                title: {
                  display: true,
                  text: "Date & Time"
                }
              },
              y: {
                beginAtZero: true,
                title: {
                  display: true,
                  text: "Temperature"
                }
              }
            },
            plugins: {
              zoom: {
                zoom: {
                  wheel: {
                    enabled: true
                  },
                  mode: "x"
                },
                pan: {
                  enabled: true,
                  mode: "x",
                },
                limits: {
                  x: { min: "original", max: "original" },
                  y: { min: "original", max: "original" }
                }
              }
            },
          }}
          height={window.innerWidth < 600 ? 400 : 1000}
          width={window.innerWidth < 600 ? 400 : 100}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { temperatures, loading, error } = state.tempReducer
  // const test = state.tempReducer
  // console.log(test)
  return {
    temperatures,
    loading,
    error
  }
}

export default connect(mapStateToProps, { getTemperature })(ConnectLineChart);