import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getEvent } from '../hook/actions/eventAction'

class ConnectedCard extends Component {
    componentDidMount() {
        this.props.getEvent()
    }
    render() {
        const { events, loading, error } = this.props
        const overflowScroll = {
            height: '800px',
        };
        return (
            <div style={overflowScroll} className='overflow-auto'>
                {loading && <div>LOADING ....</div>}
                {error && <div>{error}</div>}
                {events.reverse().map(el =>
                    <React.Fragment key={el._id}>
                        <div className='card m-4' >
                            <div className='card-body'>
                                <div className='row align-items-center'>
                                    <div className='col col-2'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-calendar-event" viewBox="0 0 16 16">
                                            <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z"/>
                                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                        </svg>
                                    </div>
                                    <div className='col col-auto align-bottom '> 
                                        <p className='m-0'>{`${el.date.substr(0, 10)} ${el.date.substr(11, 8)}`}</p>
                                    </div>
                                </div>
                                <hr/>   
                                <h5>{el.title}</h5>
                                <p><b>Location: </b>{el.location}</p>
                                <p><b>Participant:</b> {el.participants}</p>
                                <p><b>Notes:</b> {el.notes}</p>
                            </div>
                        </div>
                    </React.Fragment>
                )}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const { events, loading, error } = state.eventReducer
    // const test = state.eventReducer
    // console.log(test)
    return {
        events,
        loading,
        error
    }
}

export default connect(mapStateToProps, { getEvent })(ConnectedCard);