import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Formik, Form } from 'formik' 
import { Modal } from "react-bootstrap";
import { InputField, TextArea, AutocompleteField } from './reuse/TextField';
import { addEvent } from '../hook/actions/eventAction';
import * as Yup from 'yup';
 
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import '../assets/css/form.css';

toast.configure()
function mapDispatchToProps(dispatch) {
    return {
        addEvent: events => dispatch(addEvent(events))
    };
}

class FormModal extends Component {
    render() {  
        
        const callToast = (item) => {
            toast(`${item} Event Card created !`, {position: toast.POSITION.BOTTOM_RIGHT})
        }
        const isCancelling = () => {
            this.props.onCancel()
        }

        const { events } = this.props
        let participants = events.reverse().map(el => el.participants)

        const validate = Yup.object({
            date: Yup.date()
                .required('Date is required'),
            title: Yup.string()
                .max(30, 'Must be 30 characters or less')
                .required('Title is required'),
            location: Yup.string()
                .max(50, 'Must be 50 characters or less')
                .required('location is required'),
            participants: Yup.string()
                .max(100, 'Must be 100 characters or less')
                .required('Participants is required'),
            notes: Yup.string()
                .min(50, 'Must be longer than 50 characters')
                .required('Notes is required'),
        })
        return (
            <Modal 
                size={this.props.size}
                show={this.props.show} 
            >
                <Formik
                    initialValues={{
                        date: '',
                        title: '',
                        location: '',
                        participants: '',
                        notes: '',
                    }} 
                    validationSchema={validate}
                    onSubmit={ (values, { resetForm }) => {
                        this.props.addEvent(values); 
                        resetForm();
                        isCancelling();
                        callToast(values.title);
                    }}
                >
                    {({formik, isSubmitting }) => (
                        <div className='m-4'>
                            <h1 className='my-4 font-weight-bold-display-4'>New Event</h1>
                            <Form>
                                <InputField label='Date' name='date' type='datetime-local' />
                                <InputField label='Title' name='title' type='text' />
                                <InputField label='Location' name='location' type='text' /> 
                                <InputField label='Participants' name='participants' type='text' /> 
                                <TextArea label='Notes' name='notes' />

                                <div className='py-2 d-flex justify-content-end'>
                                    <button className='mx-4 btn btn-outline-primary' type='reset' onClick={this.props.onCancel}>Cancel</button>
                                    <button className='btn btn-primary shadow bg-primary rounded' 
                                        type='submit' disabled={isSubmitting}
                                    >Submit</button>
                                </div>
                            </Form>
                        </div>
                    )}
                </Formik>
            </Modal>
        );
    }
};

const mapStateToProps = (state) => {
    const { events } = state.eventReducer
    // const test = state.eventReducer
    // console.log(test)
    return { events }
 };

export default connect(mapStateToProps, mapDispatchToProps)(FormModal);