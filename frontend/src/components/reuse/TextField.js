import React from 'react'
import { useField, ErrorMessage } from 'formik' 

export const InputField = ({ label, ...props }) => {
    const [field, meta] = useField(props); 
    return (
        <div className="row mb-4 align-items-center">
            <div className='col col-3 align-middle'>
                <label htmlFor={field.title}>{label}</label>
            </div>
            <div className='col'>
                {props.name === 'participants' ? 
                    <input
                        className={`form-control shadow-none ${meta.touched && meta.error && 'is-invalid'}`}
                        {...field}{...props} />
                :   <input
                        className={`form-control shadow-none ${meta.touched && meta.error && 'is-invalid'}`}
                        {...field}{...props} autoComplete='off'/>}
                
                <ErrorMessage className='error' component='div' name={field.name}></ErrorMessage>
            </div>
        </div>
    )
}

export const TextArea = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
        <div className="row mb-4 align-items-center">
            <div className='col col-3 align-middle'>
                <label htmlFor={field.title}>{label}</label>
            </div>
            <div className='col'>
                <textarea
                    className={`form-control shadow-none ${meta.touched && meta.error && 'is-invalid'}`}
                    {...field}{...props}
                    autoComplete='off'
                ></textarea>
                <ErrorMessage className='error' component='div' name={field.name}></ErrorMessage>
            </div>
        </div>
    )
} 