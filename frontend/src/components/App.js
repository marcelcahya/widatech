import React, { useState } from "react";
import FormModal from "./Form"
import CardList from "./CardList"
import LineChart from "./LineChart" 

const App = () => {

    const [isOpen, setOpen] = useState(false); 

    return( 
        <div className="row m-3 align-items-center">
            <h2 className="text-center">Dashboard Web App</h2>
            <div className="col col-4">
                <div className="m-4">
                    <button className="btn btn-primary shadow bg-primary rounded" 
                        onClick={() => setOpen(true)}> Add New Events</button>
                </div>
                { isOpen ? <FormModal 
                    onCancel={() => setOpen(false)}
                    show
                /> : null }   
                <CardList /> 
            </div>
            <div className="col col-8">
                <LineChart className="w-75" />
            </div>
        </div> 
    )
}

export default App;